import React from "react";
import { observer, inject } from "mobx-react";
import {View,ActivityIndicator,FlatList,ScrollView} from "react-native";
import { Container,Button, Header, Content, Tab, Icon, Tabs } from "native-base";
import {APP_NAME} from "../../constants";
import TankerList from "./TankerList";
import TopBrandList from "./TopBrands";
@inject(["ThemeStore"],["TankStore"],["UserStore"])
@observer
export default class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation,screenProps }) => ({
      title:APP_NAME,
      headerRight:<View style={{flex:1,flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
          <Icon name="search"  style={{color:screenProps.iconColor.color,marginRight:10,fontSize:20}} />
          <Icon name="heart" style={{color:screenProps.iconColor.color,marginRight:10,fontSize:20}} /><Icon name="more"  onPress={()=>navigation.navigate("Settings")} style={{color:screenProps.iconColor.color,marginRight:10,fontSize:20}} /></View>,
  });
  constructor(props){
      super(props);
  }
  componentWillMount(){
      const UserStore=this.props.UserStore;
      UserStore.getCustomerLocation();
  }
  componentDidMount(){
  }
  render() {
      const ThemeStore=this.props.ThemeStore;
      const navigation=this.props.navigation;
      return (
          <Container>
              <Tabs>
                  <Tab tabStyle={{backgroundColor: ThemeStore.contentBackgroundColor}} textStyle={{color: ThemeStore.textColor}} activeTabStyle={{backgroundColor: ThemeStore.contentBackgroundColor}} activeTextStyle={{color: ThemeStore.textColor}} heading="TANKERS">
                      <TankerList navigation={navigation}/>
                  </Tab>
                  <Tab tabStyle={{backgroundColor: ThemeStore.contentBackgroundColor}} textStyle={{color: ThemeStore.textColor}} activeTabStyle={{backgroundColor: ThemeStore.contentBackgroundColor}} activeTextStyle={{color: ThemeStore.textColor}} heading="TOPBRANDS">
                      <TopBrandList navigation={navigation}/>
                  </Tab>
                  <Tab tabStyle={{backgroundColor: ThemeStore.contentBackgroundColor}} textStyle={{color: ThemeStore.textColor}} activeTabStyle={{backgroundColor: ThemeStore.contentBackgroundColor}} activeTextStyle={{color: ThemeStore.textColor}} heading="BUBBLETOP">
                      <TankerList navigation={navigation}/>
                  </Tab>
              </Tabs>
          </Container>
      );
  }
}
