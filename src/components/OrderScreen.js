import React from "react";
import { observer, inject } from "mobx-react";
import {View,Image,ScrollView} from "react-native";
import {Button,Text,Body,Item,DeckSwiper,Icon,Picker,Left,Thumbnail,Container,Content,Card,CardItem,Label,Segment} from "native-base";
import {APP_NAME} from "../../constants";
import PhoneInput from "react-native-phone-input";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import Fa from "react-native-vector-icons/FontAwesome";
const cards = [
    {
        text: "Ganesha Suppliers",
        name: "Coca Cola Water ",
        area:"Hitech City",
        image: {uri:"https://www.coca-colacompany.com/content/dam/journey/us/en/global/2012/10/ProductsWater-604-st-604-337-3df7dd58.rendition.598.336.jpg"},
        thumnail: {uri:"http://dev.oasiz.in:3030/assets/serviceProviders/SP01/sri-sai-ganesh-water-suppliers.png"},
    },
    {
        text: "Ganesha Suppliers",
        name: "Water Can ",
        area:"Hitech City",
        image: {uri:"http://www.alliedpurchasing.com/images/bottled-water.png"},
        thumnail: {uri:"http://dev.oasiz.in:3030/assets/serviceProviders/SP01/sri-sai-ganesh-water-suppliers.png"},
    },
    {
        text: "Ganesha Suppliers",
        name: "Aqua Fina ",
        area:"Hitech City",
        image: {uri:"https://cdni.rt.com/files/2015.10/article/5630e7a4c461887e788b4576.jpg"},
        thumnail: {uri:"http://dev.oasiz.in:3030/assets/serviceProviders/SP01/sri-sai-ganesh-water-suppliers.png"},
    },
    {
        text: "Ganesha Suppliers",
        name: "Bisleri Water Bottles ",
        area:"Hitech City",
        image: {uri:"http://www.afaqs.com/all/news/images/news_story_grfx/2017/08/51204/1-big-Bisleri.jpg"},
        thumnail: {uri:"http://dev.oasiz.in:3030/assets/serviceProviders/SP01/sri-sai-ganesh-water-suppliers.png"},
    },
];
@inject(["ThemeStore"],["UserStore"])
@observer
export default class OrderScreen extends React.Component{
  static navigationOptions = ({ navigation,screenProps }) => ({
  });
  static navigationOptions = ({ navigation,screenProps }) => ({
      title:APP_NAME,
      headerLeft:<Button transparent onPress={()=>navigation.goBack()} style={{width:100}}><Icon name="ios-arrow-back"  style={{color:screenProps.iconColor.color,marginLeft:5,fontSize:20,padding:10}} /></Button>,
      headerRight: <View  style={{marginLeft:5}} />
  });
  constructor(props){
      super(props);
  }
  state = {
      quantity:[2],
      empty_can:[2],
      delivery:"9am - 1pm",
      payment:"cash",
      selected: "9am - 1pm"
  };
  componentWillMount() {
  }
  componentDidMount(){
  }
  placeOrder(){
      const UserStore=this.props.UserStore;
      const navigation=this.props.navigation;
      const CustomerLocationObj=UserStore.customerLocation;
      const serviceProviderObj=navigation.state.params.data; // This data is from pervios screen.Passed As data Param
      let post_json={
          "serviceID": "string",
          "serviceType": "string",
          "serviceItemBrand": "string",
          "customer": "string",
          "customerContactNumber": "string",
          "customerLocationID": "string",
          "deliveryDay": "string",
          "deliveryOn": this.state.selected, // Delivery time is saving as selected of current state.
          "quantity": this.state.quantity[0],
          "totalPrice": 0,
          "serviceProvider": "string",
          "serviceProviderID": "string",
          "customerHouseNumber": "string",
          "customerAddress": "string",
          "customerLandmark": "string",
          "customerArea": "string",
          "customerAreaLocality": "string",
          "customerZip": "string",
          "customerCity": "string",
          "customerState": "string"
      };
      UserStore.placeOrder(post_json);
      navigation.navigate("PaymentScreen");
  }
  multiSliderValuesChange = (values) => {
  }
  render() {
      const ThemeStore=this.props.ThemeStore;
      const navigation=this.props.navigation;
      return(
          <Container>
              <View style={{flex:1,flexDirection:"column"}}>
                  <DeckSwiper
                      ref={(c) => this._deckSwiper = c}
                      onSwipeRight={()=>this.setState({quantity:[2],selected:"9am - 1pm"})}
                      onSwipeLeft={()=>this.setState({quantity:[2],selected:"9am - 1pm"})}
                      dataSource={cards}
                      renderEmpty={() =>
                          <View style={{ alignSelf: "center" }}>
                              <Text>Over</Text>
                          </View>
                      }
                      renderItem={item =>
                          <Card style={{ elevation: 3 }}>
                              <CardItem>
                                  <Left>
                                      <Thumbnail source={item.thumnail} />
                                      <Body>
                                          <Text>{item.text}</Text>
                                          <Text note>{item.area}</Text>
                                      </Body>
                                  </Left>
                              </CardItem>
                              <CardItem cardBody>
                                  <Image style={{ height: 200, flex: 1 }} source={item.image} />
                              </CardItem>
                              <CardItem>
                                  <Icon name="heart" style={{ color: "#ED4A6A" }} />
                                  <Text>{item.name}</Text>
                              </CardItem>
                          </Card>
                      }
                  />
              </View>
              <ScrollView style={{flex:1,marginTop:"5%"}}>
                  <Content style={{marginTop:50}}>
                      <Card style={{height:100}}>
                          <CardItem style={{flexDirection:"row"}}>
                              <Label style={{flex:1}}>Quantity - {this.state.quantity[0]}</Label>
                              <ScrollView style={{flex:1,paddingTop:"10%"}}>
                                  <MultiSlider
                                      values={[this.state.quantity[0]]}
                                      min={1}
                                      max={100}
                                      onValuesChange={(values)=>this.setState({
                                          quantity: values,
                                      })}
                                      selectedStyle={{
                                          backgroundColor: "gold",
                                      }}
                                      unselectedStyle={{
                                          backgroundColor: "silver",
                                      }}
                                      containerStyle={{
                                          height:40,
                                      }}
                                      trackStyle={{
                                          height:10,
                                          backgroundColor: "red",
                                      }}
                                      touchDimensions={{
                                          height: 40,
                                          width: 40,
                                          borderRadius: 20,
                                          slipDisplacement: 40,
                                      }}
                                  />
                              </ScrollView>
                          </CardItem>
                      </Card>
                      <Card>
                          <CardItem style={{flex:1,flexDirection:"row"}}>
                              <Label>Delivery on </Label>
                              <Picker
                                  mode="dropdown"
                                  iosHeader="Delivery On"
                                  iosIcon={<Icon name="ios-arrow-down-outline" />}
                                  //  style={{ width: undefined }}
                                  selectedValue={this.state.selected}
                                  onValueChange={(val)=>this.setState({selected:val})}
                              >
                                  <Picker.Item label="9am - 1pm" value="9am - 1pm" />
                                  <Picker.Item label="1pm - 5pm" value="1pm - 5pm" />
                                  <Picker.Item label="5pm - 9pm" value="5pm - 9pm" />
                              </Picker>
                          </CardItem>
                      </Card>
                      <View style={{flex:1,flexDirection:"row",alignItems:"center",justifyContent:"center",marginTop:10,marginBottom:10}}>
                          <Button  onPress={()=>this.placeOrder(navigation.state.params.data)} block style={{backgroundColor:ThemeStore.buttonColors,width:"80%"}}>
                              <Text>Order </Text>
                          </Button>
                      </View>
                  </Content>
              </ScrollView>
          </Container>
      );
  }
}
